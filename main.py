from flask import Flask, render_template
from dotenv import dotenv_values
import requests
import os


config = dotenv_values(".env")
print(config)

app = Flask(__name__)

@app.route('/')
def index():
    try:
        response = requests.get(config['MOVIES_API_URL'])

        if response.status_code == 200:
            print(response.json())
            movies = response.json()

            return render_template('index.html', movies=movies)
    except requests.exceptions.RequestException as e:
        return render_template('index.html')

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)


